#include <iostream>
#include "Renderer.h"
#include "Grapic.h"
#include <thread>


int main() {

    int size=2;
    bool fluid= false;
    Renderer r(512*3+size*4,512+size*2);
    r.tileSize=size;
    r.mazeList[0] = new Prim(512 / r.tileSize, 512 / r.tileSize, fluid);
    r.mazeList[1] = new Backtrack(512 / r.tileSize, 512 / r.tileSize, fluid);
    r.mazeList[2] = new Recurs(512 / r.tileSize, 512 / r.tileSize, fluid);
    srand(time(NULL));

    bool start= false;

    while (true) {
        r.draw();
        if (!start){
            grapic::pressSpace();
            start=true;
            std::thread threadPrim(&Maze::generate,r.mazeList[0]);
            std::thread threadBT(&Maze::generate,r.mazeList[1]);
            std::thread threadRec(&Maze::generate,r.mazeList[2]);
            threadPrim.detach();
            threadBT.detach();
            threadRec.detach();
        } else if (fluid){
            r.mazeList[0]->waitCV.notify_one();
            r.mazeList[1]->waitCV.notify_one();
            r.mazeList[2]->waitCV.notify_one();
        }
        //grapic::delay(500);
    }

    grapic::pressSpace();
    return 0;
}
