//
// Created by constantin on 15/02/2021.
//

#include "Maze.h"
#include "Grapic.h"


Maze::Maze(const int &_x, const int &_y) {
    gridPtr = new Grid(_x, _y);
    mazeSolver = new Solver(this);
}

void Prim::generate() {
    gridPtr->tileMap[0].isExplored = true;
    gridPtr->tileMap[0].isVisited = true;
    addWall();
    position= Pos();

    while(!wallVector.empty()){
        if (visibleGen) {
            std::unique_lock<std::mutex> lck{waitMtx};
            waitCV.wait(lck);
            lck.unlock();
        }
        int randWall= grapic::irand(0,wallVector.size()-1);
        position=wallVector[randWall];
        wallVector.erase(wallVector.begin()+randWall);
        if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y+1)) + (position.x)].isExplored &&
            gridPtr->tileMap[(gridPtr->sizeX * (position.y-1)) + (position.x)].isExplored &&
            position.y+1 < gridPtr->sizeY&&
            position.y-1 >=0
            ){
            gridPtr->tileMap[(gridPtr->sizeX * (position.y+1)) + (position.x)].isExplored=true;
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x)].isExplored=true;
            position.y+=1;
            addWall();
        } else if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y-1)) + (position.x)].isExplored &&
                    gridPtr->tileMap[(gridPtr->sizeX * (position.y+1)) + (position.x)].isExplored &&
                    position.y+1 < gridPtr->sizeY&&
                   position.y-1 >=0
                ){
            gridPtr->tileMap[(gridPtr->sizeX * (position.y-1)) + (position.x)].isExplored=true;
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x)].isExplored=true;
            position.y-=1;
            addWall();
        } else if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x+1)].isExplored &&
                    gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x-1)].isExplored &&
                    position.x+1 < gridPtr->sizeX &&
                   position.x-1 >=0
                ){
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x+1)].isExplored=true;
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x)].isExplored=true;
            position.x+=1;
            addWall();
        } else if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x-1)].isExplored &&
                    gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x+1)].isExplored &&
                    position.x+1 < gridPtr->sizeX &&
                   position.x-1 >=0
                ){
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x-1)].isExplored= true;
            gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x)].isExplored= true;
            position.x-=1;
            addWall();
        }
    }
    mazeSolver->solve();
}

Prim::Prim(const int &_x, const int &_y, const bool &_visibleGen) : Maze(_x, _y) {
    visibleGen = _visibleGen;
}

void Prim::addWall() {
    if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y+1)) + (position.x)].isExplored &&
        !gridPtr->tileMap[(gridPtr->sizeX * (position.y+2)) + (position.x)].isExplored &&
        position.y+2 < gridPtr->sizeY
        ){
        wallVector.push_back(Pos{position.x,position.y+1});
    }

    if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y-1)) + (position.x)].isExplored &&
        !gridPtr->tileMap[(gridPtr->sizeX * (position.y-2)) + (position.x)].isExplored &&
        position.y-2 >=0
            ){
        wallVector.push_back(Pos{position.x,position.y-1});
    }

    if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x+1)].isExplored &&
        !gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x+2)].isExplored &&
        position.x+2 < gridPtr->sizeX
            ){
        wallVector.push_back(Pos{position.x+1,position.y});
    }

    if (!gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x-1)].isExplored &&
        !gridPtr->tileMap[(gridPtr->sizeX * (position.y)) + (position.x-2)].isExplored &&
        position.x-2 >= 0
            ){
        wallVector.push_back(Pos{position.x-1,position.y});
    }
}

void Backtrack::generate() {
    int posX = 0, posY = 0;
    gridPtr->tileMap[0].isExplored = true;
    gridPtr->tileMap[0].isVisited = true;
    backtrackX.push(posX);
    backtrackY.push(posY);
    //backtrackX.push(posX);
    //backtrackY.push(posY);

    while (!backtrackX.empty()) {
        //std::cout << "am doin dis" << std::endl;
        //grapic::delay(100);
        if (visibleGen) {
            std::unique_lock<std::mutex> lck{waitMtx};
            waitCV.wait(lck);
            lck.unlock();
        }
        int dir;
        bool stuck[4]{false, false, false, false};
        if (true) {
            bool moved = false;
            while (!moved) {
                dir = grapic::irand(0, 3);
                //std::cout << "case : " << dir << " " << posX << ", " << posY << std::endl;
                switch (dir) {
                    case 1: {
                        if (!gridPtr->tileMap[(gridPtr->sizeX * (posY - 2)) + (posX)].isExplored && posY - 2 >= 0) {
                            backtrackX.push(posX);
                            backtrackY.push(posY);
                            posY -= 2;

                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX)].isExplored = true;
                            gridPtr->tileMap[(gridPtr->sizeX * (posY + 1)) + (posX)].isExplored = true;
                            moved = true;
                        } else stuck[dir] = true;
                    }
                    case 2: {
                        if (!gridPtr->tileMap[(gridPtr->sizeX * (posY + 2)) + (posX)].isExplored &&
                            posY + 2 < gridPtr->sizeY) {
                            backtrackX.push(posX);
                            backtrackY.push(posY);
                            posY += 2;

                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX)].isExplored = true;
                            gridPtr->tileMap[(gridPtr->sizeX * (posY - 1)) + (posX)].isExplored = true;
                            moved = true;
                        } else stuck[dir] = true;
                    }
                    case 3: {
                        if (!gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX - 2)].isExplored && posX - 2 >= 0) {
                            backtrackX.push(posX);
                            backtrackY.push(posY);
                            posX -= 2;

                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX)].isExplored = true;
                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX + 1)].isExplored = true;
                            moved = true;
                        } else stuck[dir] = true;
                    }
                    case 0: {
                        if (!gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX + 2)].isExplored &&
                            posX + 2 < gridPtr->sizeX) {
                            backtrackX.push(posX);
                            backtrackY.push(posY);
                            posX += 2;

                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX)].isExplored = true;
                            gridPtr->tileMap[(gridPtr->sizeX * (posY)) + (posX - 1)].isExplored = true;

                            moved = true;
                        } else stuck[dir] = true;
                    }
                    default: {
                    }

                        if (stuck[0] && stuck[1] && stuck[2] && stuck[3]) {
                            //std::cout << "go back" << std::endl;

                            moved = true;
                            posX = backtrackX.top();
                            posY = backtrackY.top();
                            backtrackX.pop();
                            backtrackY.pop();
                        }
                }
            }
        }
    }
    //std::cout << "FINIS BACKTRACK" << std::endl;
    mazeSolver->solve();
}

Backtrack::Backtrack(const int &_x, const int &_y, const bool &_visibleGen) : Maze(_x, _y) {
    visibleGen = _visibleGen;
}

void Recurs::generate() {
    for (int xI = 0; xI < gridPtr->sizeX-1 ; ++xI) {
        for (int yI = 0; yI < gridPtr->sizeY-1; ++yI) {
            //gridPtr->tileMap[(gridPtr->sizeX * (yI)) + (xI)].isVisited=true;
            gridPtr->tileMap[(gridPtr->sizeX * (yI)) + (xI)].isExplored=true;
        }
    }

    recursive(gridPtr->sizeX, gridPtr->sizeY, 0, 0);

    mazeSolver->solve();
}

Recurs::Recurs(const int &_x, const int &_y, const bool &_visibleGen) : Maze(_x, _y) {
    currentH=_y;
    currentW=_x;
    visibleGen=_visibleGen;
    gridPtr->tileMap[(gridPtr->sizeX * 0) + (0)].isVisited= true;
}

void Recurs::recursive(const int &_sizeX, const int &_sizeY, const int &_offsetX,const int & _offsetY) {
    bool horizontal = grapic::irand(0,100)%2;
    if (visibleGen) {
        std::unique_lock<std::mutex> lck{waitMtx};
        waitCV.wait(lck);
        lck.unlock();
    }

    if (_sizeX > _sizeY){
        horizontal= false;
    } else if(_sizeY> _sizeX) horizontal= true;
    else horizontal = grapic::irand(0,100)%2;

    if (_sizeX >2 && _sizeY >2){
        int rand;
        if(horizontal){
            for (int i = _offsetX; i < _sizeX+_offsetX; ++i) {
                gridPtr->tileMap[(gridPtr->sizeX * (((_sizeY+_offsetY*2)-1)/2)) + (i)].isExplored= false;
            }
            rand= grapic::irand(0, ((_sizeX-1)/2))*2;
            gridPtr->tileMap[(gridPtr->sizeX * (((_sizeY+_offsetY*2)-1)/2)) + (rand+_offsetX)].isExplored= true;

            recursive(_sizeX,_sizeY/2,_offsetX,_offsetY);
            recursive(_sizeX,_sizeY/2,_offsetX,_offsetY + _sizeY/2);
        }
        else {
            for (int i = _offsetY; i < _sizeY+_offsetY; ++i) {
                gridPtr->tileMap[(gridPtr->sizeX * (i)) + (((_sizeX+_offsetX*2)-1)/2)].isExplored= false;
            }
            rand= grapic::irand(0 , ((_sizeY-1)/2))*2;
            gridPtr->tileMap[(gridPtr->sizeX * (rand+_offsetY)) + (((_sizeX+_offsetX*2)-2)/2)].isExplored= true;

            recursive(_sizeX/2,_sizeY,_offsetX,_offsetY);
            recursive(_sizeX/2,_sizeY,_offsetX+_sizeX/2,_offsetY);
        }




    }
}

Pos::Pos(int _x, int _y) {
    x=_x;
    y=_y;
}
