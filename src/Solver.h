//
// Created by constantin on 16/02/2021.
//

#ifndef LABYY_SOLVER_H
#define LABYY_SOLVER_H
#include "Maze.h"
#include <stack>

struct Pos;

class Maze;

class Solver {
public:
    Maze* toSolve;
    std::stack<Pos> posStack;

    Solver(Maze* _mazeRef);
    void solve();


private:
    bool solved=false;
    void recursive(Pos _position, const Pos &_exit);
};


#endif //LABYY_SOLVER_H
