//
// Created by constantin on 15/02/2021.
//

#ifndef LABYY_MAZE_H
#define LABYY_MAZE_H

#include "Grid.h"
#include "Solver.h"
#include <stack>
#include <mutex>
#include <condition_variable>

struct Pos{
    Pos(int _x=0, int _y=0);

    int x=0;
    int y=0;
};

class Solver;

class Maze {
public:
    Grid * gridPtr;
    Solver * mazeSolver;
    Maze(const int &_x, const int &_y);
    virtual void generate()=0;
    bool visibleGen;
    std::mutex waitMtx;
    std::condition_variable waitCV;
};

class Prim : public Maze {
public:

    Prim(const int &_x, const int &_y, const bool &_visibleGen);
    void generate() override;
    void addWall();
    Pos position;
    std::vector<Pos> wallVector;

};

class Backtrack : public Maze {
public:
    std::stack<int> backtrackX;
    std::stack<int> backtrackY;


    Backtrack(const int &_x, const int &_y, const bool &_visibleGen);
    void generate() override;
};

class Recurs : public Maze{
public:

    int currentW;
    int currentH;
    Recurs(const int &_x, const int &_y, const bool &_visibleGen);
    void generate() override;
    void recursive(const int &_sizeX, const int &_sizeY, const int &_offsetX,const int& _offestY);
};

#endif //LABYY_MAZE_H
