//
// Created by constantin on 15/02/2021.
//
#include <iostream>
#include "Grid.h"

Grid::Grid(const int &_sizeX, const int &_sizeY) {
    sizeX=_sizeX;
    sizeY=_sizeY;
    tileMap=new Tile[sizeX * sizeY];

    for (int i = 0; i < sizeX*sizeY ; ++i) {
        if (tileMap[i].isExplored){
            std::cout<<"pas bon"<<std::endl;
        }
    }
}

Grid::~Grid() {
    delete [] tileMap;
}
