//
// Created by constantin on 25/01/2021.
//

#ifndef ATRIA_RENDERER_H
#define ATRIA_RENDERER_H


#include "Maze.h"

class Renderer {
public:
    int tileSize=4;
    Renderer(const unsigned int & _sizeX, const unsigned int & _sizeY);
    void draw() const;
    Maze* mazeList[3];

private:
    int x,y;
    int r,g,b;
};


#endif //ATRIA_RENDERER_H
