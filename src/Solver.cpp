//
// Created by constantin on 16/02/2021.
//

#include "Solver.h"
#include <iostream>

Solver::Solver(Maze *_mazeRef) {
    toSolve = _mazeRef;
}

void Solver::solve() {
    Pos start, prev(-1, -1);
    posStack.push(start);
    toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (start.y)) + (start.x)].isVisited = true;
    toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (start.y)) + (start.x)].isPath = true;

    recursive(start, Pos(toSolve->gridPtr->sizeX - 2, toSolve->gridPtr->sizeY - 2));
}

void Solver::recursive(Pos _position, const Pos &_exit) {
    if ((true) || !solved) {
        //_position=posStack.top();
        //_previousPos.x++;
        //std::cout << _position.x << ", " << _position.y << "//" << _exit.x << ", " << _exit.y<< std::endl;


        if (toSolve->visibleGen) {
            std::unique_lock<std::mutex> lck{toSolve->waitMtx};
            toSolve->waitCV.wait(lck);
            lck.unlock();
        }
        Pos workingPos = _position;//posStack.top();

        toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x)].isVisited = true;
        toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x)].isPath = true;

        if (workingPos.x == _exit.x && workingPos.y == _exit.y) {
            solved = true;
        } else {
            if ((workingPos.x != _exit.x || workingPos.y!= _exit.y) &&
                workingPos.y + 1 < toSolve->gridPtr->sizeY - 1 &&
                !toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y + 1)) + (workingPos.x)].isVisited &&
                toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y + 1)) + (workingPos.x)].isExplored && !solved
                    ) {
                //std::cout << "up" << std::endl;
                Pos newWorking(workingPos.x,workingPos.y + 1);

                //posStack.push(newWorking);
                recursive(newWorking, _exit);

            }

            if ((workingPos.x!= _exit.x || workingPos.y != _exit.y) &&
                workingPos.x + 1 < toSolve->gridPtr->sizeX - 1 &&
                !toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x + 1)].isVisited &&
                toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x + 1)].isExplored && !solved
                    ) {
                //std::cout << "right" << std::endl;
                Pos newWorking(workingPos.x+1,workingPos.y);

                //posStack.push(newWorking);
                recursive(newWorking, _exit);
            }

            if ((workingPos.x != _exit.x || workingPos.y!= _exit.y) &&
                workingPos.y - 1 >=0 &&
                !toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y - 1)) + (workingPos.x)].isVisited &&
                toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y - 1)) + (workingPos.x)].isExplored && !solved
                    ) {
                //std::cout << "down" << std::endl;
                Pos newWorking(workingPos.x,workingPos.y - 1);

                //posStack.push(newWorking);
                recursive(newWorking, _exit);

            }

            if ((workingPos.x!= _exit.x || workingPos.y != _exit.y) &&
                workingPos.x - 1 >=0 &&
                !toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x - 1)].isVisited &&
                toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x - 1)].isExplored && !solved
                    ) {
                //std::cout << "left" << std::endl;
                Pos newWorking(workingPos.x-1,workingPos.y);

                //posStack.push(newWorking);
                recursive(newWorking, _exit);
            }


            if (!solved){
                //std::cout<<posStack.top().x<<", "<<posStack.top().y<<" poped"<<std::endl;
                toSolve->gridPtr->tileMap[(toSolve->gridPtr->sizeX * (workingPos.y)) + (workingPos.x)].isPath = false;
                //posStack.pop();
            }
        }
    }
}
