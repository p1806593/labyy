//
// Created by constantin on 15/02/2021.
//

#ifndef LABYY_GRID_H
#define LABYY_GRID_H

#include <vector>

class Tile{
public:
    bool isExplored = false;
    bool isVisited = false;
    bool isPath= false;
};

class Grid {
public:
    int sizeX, sizeY;
    Tile* tileMap;

    Grid(const int &_sizeX, const int &_sizeY);
    ~Grid();
};


#endif //LABYY_GRID_H
