//
// Created by constantin on 25/01/2021.
//

#include "Renderer.h"
#include "iostream"
#include "Grapic.h"

void Renderer::draw() const {
    grapic::winClear();

    for (int xI = 0 , effectiveX=x/tileSize; xI < effectiveX; ++xI) {
        for (int yI = 0, effectiveY=y/tileSize; yI < effectiveY; ++yI) {
            if (xI%(effectiveX/3)==0 || xI==effectiveX-1 || yI==0 || yI==effectiveY-1) {
                //std::cout<<effectiveX/3<<std::endl;
                grapic::color(0, 0, 0,255);
                grapic::rectangleFill(
                        xI*tileSize,
                        yI*tileSize,
                        (xI+1)*tileSize,
                        (yI+1)*tileSize
                        );
            } else{
                //grapic::print(xI*tileSize+4,yI*tileSize+2,xI%(effectiveX/3));
            }
        }
    }
    int offset=1;
    int index=0;
    for (auto &m : mazeList) {
        //std::cout<<"maze : "<<offset<<std::endl;
        int xR=0,yR=0;
        for (int xI = offset; xI < m->gridPtr->sizeX + offset; ++xI) {
            for (int yI = 1; yI < m->gridPtr->sizeY + 1; ++yI) {
                if (m->gridPtr->tileMap[(m->gridPtr->sizeX * (yI-1)) + (xI-offset)].isPath){
                    grapic::color(0,255,0);
                } else if (m->gridPtr->tileMap[(m->gridPtr->sizeX * (yI-1)) + (xI-offset)].isExplored){
                    grapic::color(255,255,255);
                } else{
                    grapic::color(50,50,50,255);
                }
                grapic::rectangleFill(
                        xI*tileSize,
                        yI*tileSize,
                        (xI+1)*tileSize,
                        (yI+1)*tileSize
                );

                //std::cout<<xI<<std::endl;

                yR++;
            }
            xR++;
        }
        offset +=512/tileSize+1;
    }



    //grapic::rectangleFill(200,200,700,500);
    //TODO

    grapic::winDisplay();
}

Renderer::Renderer(const unsigned int & _sizeX, const unsigned int & _sizeY) {
    grapic::winInit("Laby", _sizeX, _sizeY);
    grapic::backgroundColor(120, 120, 120);
    grapic::fontSize(10);
    x=_sizeX;
    y=_sizeY;
}

